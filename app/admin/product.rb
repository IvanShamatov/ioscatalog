ActiveAdmin.register Product do

  scope_to :current_admin_user

  index do
    column :name
    column :link
    column :category
    column :price
    column :currency
    column :store
    default_actions
  end


  form do |f|
    f.inputs do
      f.input :name
      f.input :picture
      f.input :link
      f.input :price
      f.input :currency
      f.input :category, as: :select, collection: Product.by_user(current_admin_user.id)
      f.input :store, as: :select, collection: Store.by_user(current_admin_user.id)
    end
    f.actions do
      f.submit
    end
  end


  show do
    attributes_table do 
      row :name
      row :link
      row :price
      row :currency
      row :category
      row :picture
      row :store
    end
  end


  controller do 
    def permitted_params
      params.permit product: [:name, :link, :price, :picture, :category_id, :currency, :store_id]
    end

    def scoped_collection
      Product.by_user(current_admin_user.id)
    end
  end
end
