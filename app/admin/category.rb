ActiveAdmin.register Category do

  scope_to :current_admin_user 

  # belongs_to :store

  config.clear_action_items!

  index do
    column :name
    column :ancestry
    default_actions
  end


  form do |f|
    f.inputs do
      f.input :name
      f.input :parent_id, as: :select, collection: Category.by_user(current_admin_user.id)
      f.input :store, as: :select, collection: Store.by_user(current_admin_user.id)
    end
    f.actions do
      f.submit
    end
  end


  controller do 
    def permitted_params
      params.permit category: [:name, :parent_id, :store_id]
    end

    def scoped_collection
      Category.by_user(current_admin_user.id)
    end
  end
end
