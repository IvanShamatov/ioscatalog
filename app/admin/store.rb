ActiveAdmin.register Store do
  menu priority: 1
  scope_to :current_admin_user

  index do
    column :name
    column :application_key
    # column :pricelist do |store|
    #   str = store.pricelists.empty? ? "upload" : "update"
    #   link_to str, new_admin_store_pricelist_path(store.id)
    # end
    default_actions
  end


  form do |f|
    f.inputs "Store details" do
      f.input :name
    end
    f.actions
  end


  controller do
    def permitted_params
      params.permit store: :name
    end

    def create
      puts params.inspect
      Store.create(name: params[:store][:name], admin_user: current_admin_user)
      redirect_to admin_stores_path
    end
  end
end
