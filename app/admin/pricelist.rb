ActiveAdmin.register Pricelist do
	belongs_to :store

	form do |f|
		f.inputs do
			f.input :file
			f.input :parser, as: :select, collection: Pricelist.parsers
		end
		f.actions do
			f.submit
		end
	end

	controller do 
    def permitted_params
      params.permit pricelist: [:file, :store_id, :parser]
    end
  end
end
