class BinpressValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    secret = Settings.binpress.secret
    key = Settings.binpress.key
    email = value.gsub('@', '%40')
    url = 'component_id=1271&email='+email+'&apikey='+key
    sig = Digest::MD5.hexdigest(secret+url)
    link = 'http://binpress.com/api/publishers/purchases?'+url+'&sig='+sig
    json = JSON.parse(RestClient.get link)

    if json["purchases"].count == 0
      record.errors[attribute] << (options[:message] || "Registration available for only binpress customers")
    end
  end
end