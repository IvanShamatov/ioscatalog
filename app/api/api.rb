class API < Grape::API
  format :json

  segment '/:app_key' do
    resources :category do

      get '/tree' do
        store = Store.includes(:categories).find_by(application_key: params[:app_key])
        categories = store.categories
        json_obj = Jbuilder.encode do |json|
          json.array! categories do |cat|
            json.category_id cat.id
            json.name cat.name
            json.parent_id cat.parent ? cat.parent.id : nil
          end
        end
        JSON.parse(json_obj)
      end


      get '/' do
        store = Store.find_by(application_key: params[:app_key])
        products = Product.where(store_id: store.id).offset(params[:offset]).limit(params[:limit]).order("RANDOM()")#page(params[:page])

        json_obj = Jbuilder.encode do |json|
          json.products products do |product|
            json.product_id product.id
            json.product_url product.link
            json.name product.name
            json.price product.price
            json.currency product.currency
            json.medium_image_url product.picture.medium.url
            json.small_image_url  product.picture.small.url
            json.large_image_url  product.picture.large.url
          end
        end
        JSON.parse(json_obj)
      end


      params do
        requires :id, type: Integer, desc: "Category ID."
        optional :page, type: Integer
        optional :per, type: Integer
      end
      get ':id' do
        store = Store.find_by(application_key: params[:app_key])
        category = Category.find_by(id: params[:id], store_id: store)
        raise "No category" if category.nil?
        ids = category.subtree.map(&:id)
        products = Product.where(category_id: ids).offset(params[:offset]).limit(params[:limit]).order("RANDOM()")#.page(params[:page])

        json_obj = Jbuilder.encode do |json|
          json.category do
            json.category_id category.id
            json.name category.name
            json.breadcrumbs category.path.map(&:name)
          end
          json.products products do |product|
            json.product_id product.id
            json.name product.name
            json.product_url product.link
            json.price product.price
            json.currency product.currency
            json.medium_image_url product.picture.medium.url
            json.small_image_url  product.picture.small.url
            json.large_image_url  product.picture.large.url
          end
        end
        JSON.parse(json_obj)
      end
    end


    resources :product do
      params do
        requires :id, type: Integer, desc: "Product ID."
      end
      get ':id' do
        product = Product.find_by(id: params[:id])
        json_obj = Jbuilder.encode do |json|
          json.product_id product.id
          json.name product.name
          json.product_url product.link
          json.price product.price
          json.currency product.currency
          json.medium_image_url product.picture.medium.url
          json.small_image_url  product.picture.small.url
          json.large_image_url  product.picture.large.url
        end
        JSON.parse(json_obj)
      end
    end
  end
end
