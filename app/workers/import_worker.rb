class ImportWorker
	include Sidekiq::Worker


	def perform(pricelist_id)
		list = Pricelist.find_by(id: pricelist_id)
		list.import_products
	end
end