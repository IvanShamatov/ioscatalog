class Pricelist < ActiveRecord::Base
  # Associations
  belongs_to :store

  # Callbacks
  after_create :import_async

  # Uploader
  mount_uploader :file, PricelistUploader

  # Vocabulary of available strategies
  def self.parsers
    {"Yandex.XML" => :yandex_xml}
  end


  # Starts worker
  def import_async
    ImportWorker.perform_async(self.id)
  end


  # getting importer instance
  def importer
    @importer ||= ["Importer", self.parser.camelize].join("::").constantize.new(file, self.store)
  end

  # import parts of data
  def import_data
    import_categories
    import_products
  end


  # import categories from file
  def import_categories
    importer.categories
  end

  # import products from file
  def import_products
    importer.products
  end

end
