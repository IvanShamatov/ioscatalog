class Product < ActiveRecord::Base

  # Associations
  belongs_to :category
  belongs_to :store


  # Uploader
  mount_uploader :picture, PictureUploader


  def self.by_user(id)
    joins(:store).where("stores.admin_user_id = ?", id)
  end
  
end
