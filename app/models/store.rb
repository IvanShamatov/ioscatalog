class Store < ActiveRecord::Base

  # Associations
  belongs_to :admin_user
  has_many :categories
  has_many :pricelists

  # Callbacks
  before_create :generate_application_key, :under_stores_limit


  def self.by_user(id)
    where(admin_user_id: id)
  end


  private
    def under_stores_limit
      errors.add(:name, "You reached store limit for your account") if self.admin_user.stores.count >= AdminUser::STORES_LIMIT
    end

    def generate_application_key
      begin
        self.application_key = SecureRandom.hex(24) # or whatever you chose like UUID tools
      end while self.class.exists?(:application_key => application_key)
    end
end
