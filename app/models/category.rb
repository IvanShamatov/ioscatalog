class Category < ActiveRecord::Base

  # Associations
  has_many :products
  belongs_to :store
  has_ancestry :orphan_strategy => :adopt, :cache_depth => true


  def self.by_user(id)
    joins(:store).where("stores.admin_user_id = ?", id)
  end
  
end
