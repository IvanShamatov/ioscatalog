class AddExternalIdToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :external_id, :integer
  end
end
