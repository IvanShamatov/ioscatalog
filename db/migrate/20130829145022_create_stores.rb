class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.belongs_to :admin_user, index: true
      t.string :name
      t.string :application_key

      t.timestamps
    end
  end
end
