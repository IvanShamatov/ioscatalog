class CreatePricelists < ActiveRecord::Migration
  def change
    create_table :pricelists do |t|
      t.integer :store_id
      t.string :file

      t.timestamps
    end
  end
end
